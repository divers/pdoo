<?php

require 'simpletest/autorun.php';

class AllTests extends TestSuite {
  function AllTests() {
    parent::__construct();
    foreach (glob('tests/*_test.php') as $f) $this->addFile($f);
  }
}

