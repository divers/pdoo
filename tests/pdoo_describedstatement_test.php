<?php

require_once 'simpletest/autorun.php';
require_once 'described.class.php';

class TestOfPDOODescribedStatement extends UnitTestCase {
  private $db;
  private $p;

  function __construct() {
    try {
      $this->db = new PDOO('sqlite::memory:');
      $this->db->exec(file_get_contents('tests/sqlite.test.schema'));
    } catch (PDOException $e) {
      echo $e->getMessage() . "\n";
    }
  }

  function testMake() {
    $this->p = $this->db->make('PDOODescribedStatement');
    $this->assertEqual('PDOODescribedStatement', get_class($this->p));
  }

  function testKey() {
    $this->assertEqual('id', $this->p->getKey());
  }

  function testColumns() {
    $this->assertEqual(array('name', 'description', 'created', 'modified', 'id'), $this->p->getColumns());
  }

  function testGetSet() {
    $this->assertNull($this->p->junk);
    $this->assertNull($this->p->name);
    $this->p->junk = true;
    $this->assertNull($this->p->junk);
    $this->p->name = 'Joe2';
    $this->assertEqual('Joe2', $this->p->name);
  }

  function testInsert() {
    $this->p->description = 'bla bla bla';
    $this->p->created = time();
    $this->assertTrue($this->p->insert());
    $this->assertEqual(1, $this->p->id);
    $this->assertFalse($this->p->insert());
    $this->assertNull($this->p->id);
  }

  function testSelect() {
    $this->p->select('select * from PDOODescribedStatement where id=1');
    $this->assertEqual('Joe2', $this->p->name);
    $this->assertEqual('bla bla bla', $this->p->description);
    $this->p->reset();
    $this->p->select('select * from PDOODescribedStatement where id=2');
    $this->assertNull($this->p->name);
    $this->assertNull($this->p->description);
    $this->p->select('select * from PDOODescribedStatement where name="Joe2"');
    $this->assertEqual('Joe2', $this->p->name);
    $this->assertEqual('bla bla bla', $this->p->description);
    $this->assertEqual(1, $this->p->id);
  }
}
