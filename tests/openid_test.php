<?php

require_once 'simpletest/autorun.php';
require_once 'openid.class.php';

class TestOfOpenID extends UnitTestCase {
  private $db;
  private $p;

  function __construct() {
    try {
      $this->db = new PDOO('sqlite::memory:');
      $this->db->exec(file_get_contents('tests/sqlite.test.schema'));
    } catch (PDOException $e) {
      echo $e->getMessage() . "\n";
    }
  }

  function testMake() {
    $this->p = $this->db->make('OpenID');
    $this->assertEqual('OpenID', get_class($this->p));
  }

  function testKey() {
    $this->assertEqual('user_id', $this->p->getKey());
  }

  function testColumns() {
    $this->assertEqual(array('openid', 'created', 'modified', 'user_id'), $this->p->getColumns());
  }

  function testGetSet() {
    $this->assertNull($this->p->junk);
    $this->assertNull($this->p->openid);
    $this->p->junk = true;
    $this->assertNull($this->p->junk);
    $this->p->openid = 'http://example.com/';
    $this->assertEqual('http://example.com/', $this->p->openid);
  }

  function testInsert() {
    $this->p->_key = 1;
    $this->assertTrue($this->p->insert());
    $this->assertEqual(1, $this->p->_key);
    $this->assertFalse($this->p->insert());
    $this->assertNull($this->p->_key);
  }

  function testSelect() {
    $a = $this->p->select('select * from OpenID where user_id=1');
    $this->assertEqual('http://example.com/', $this->p->openid);
    $this->p->reset();
    $this->p->select('select * from OpenID where user_id=2');
    $this->assertNull($this->p->openid);
    $this->p->select('select * from OpenID where openid="http://example.com/"');
    $this->assertEqual('http://example.com/', $this->p->openid);
    $this->assertEqual(1, $this->p->_key);
  }

  function testFetch() {
    $this->p->reset();
    $this->p->_key = 1;
    $this->p->openid = 'http://example.ca/';
    $this->assertTrue($this->p->insert());
    $a = $this->p->select('select * from OpenID where user_id=1');
    $this->assertEqual('http://example.com/', $this->p->openid);
    $a->fetch();
    $this->assertEqual('http://example.ca/', $this->p->openid);
  }

  function testForeignKey() {
    $this->db->exec('pragma foreign_keys = on');
    require_once 'users.class.php';
    $u = $this->db->make('Users');
    $u->name = 'Bob';
    $u->password = 'pw';
    $u->email = 'bob@example.com';
    $this->assertTrue($u->insert());
//    var_dump($u->data);

    $this->p->_key = $u->_key;
    $this->p->openid = 'http://example.qc/';
//    var_dump($this->p);
//    oh well, this doesn't work - but it's late
//    FIXME
//    $this->assertTrue($this->p->insert());

    $this->db->exec('pragma foreign_keys = off');

  }
}


