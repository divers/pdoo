<?php

require_once 'simpletest/autorun.php';
require_once 'pdoo.class.php';

class PDOOStatementTest extends PDOOStatement {
  protected function __construct($dbh, $columns, $key = null) {
    parent::__construct($dbh, $columns, $key);
  }

  static function defaultColumns() {
    return array('name');
  }
}

class TestOfPDOO extends UnitTestCase {
  private $db;
  private $p;

  function __construct() {
    try {
      $this->db = new PDOO('sqlite::memory:');
      $this->db->exec(file_get_contents('tests/sqlite.test.schema'));
    } catch (PDOException $e) {
      echo $e->getMessage() . "\n";
    }
  }

  function testMake() {
    $this->p = $this->db->make('PDOOStatementTest');
    $this->assertEqual('PDOOStatementTest', get_class($this->p));
  }

  function testGetTables() {
    $this->assertEqual(array('PDOOStatementTest'), $this->db->getTables());
  }

  function testGetTable() {
    $this->assertEqual($this->p, $this->db->getTable('PDOOStatementTest'));
  }

  function testKey() {
    $this->assertEqual('id', $this->p->getKey());
  }

  function testColumns() {
    $this->assertEqual(array('name', 'id'), $this->p->getColumns());
  }

  function testGetSet() {
    $this->assertNull($this->p->junk);
    $this->assertNull($this->p->name);
    $this->p->junk = true;
    $this->assertNull($this->p->junk);
    $this->p->name = 'Joe2';
    $this->assertEqual('Joe2', $this->p->name);
  }

  function testInsert() {
    $this->assertTrue($this->p->insert());
    $this->assertEqual(1, $this->p->id);
    $this->assertFalse($this->p->insert());
    $this->assertNull($this->p->id);
  }

  function testSelect() {
    $this->p->select('select * from PDOOStatementTest where id=1');
    $this->assertEqual('Joe2', $this->p->name);
    $this->p->reset();
    $this->p->select('select * from PDOOStatementTest where id=2');
    $this->assertNull($this->p->name);
    $this->p->select('select * from PDOOStatementTest where name="Joe2"');
    $this->assertEqual('Joe2', $this->p->name);
    $this->assertEqual(1, $this->p->id);
  }

  function testSelectWhere() {
    $this->p->reset();
    $this->p->selectWhere(array('id' => 1));
    $this->assertEqual('Joe2', $this->p->name);
    $this->p->reset();
    $this->p->selectWhere(array('id' => 2));
    $this->assertNull($this->p->name);
  }

  function testFill() {
    $this->p->reset();
    $this->p->id = 1;
    $this->p->fill();
    $this->assertEqual('Joe2', $this->p->name);
    $this->p->reset();
    $this->p->id = 2;
    $this->p->fill();
    $this->assertNull($this->p->name);
  }
}
