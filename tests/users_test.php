<?php

require_once 'simpletest/autorun.php';
require_once 'users.class.php';

class TestOfUsers extends UnitTestCase {
  private $db;
  private $p;

  function __construct() {
    try {
      $this->db = new PDOO('sqlite::memory:');
      $this->db->exec(file_get_contents('tests/sqlite.test.schema'));
    } catch (PDOException $e) {
      echo $e->getMessage() . "\n";
    }
  }

  function testMake() {
    $this->p = $this->db->make('Users');
    $this->assertEqual('Users', get_class($this->p));
  }

  function testKey() {
    $this->assertEqual('id', $this->p->getKey());
  }

  function testColumns() {
    $this->assertEqual(array('name', 'password', 'email', 'created', 'modified', 'id'), $this->p->getColumns());
  }

  function testGetSet() {
    $this->assertNull($this->p->junk);
    $this->assertNull($this->p->name);
    $this->p->junk = true;
    $this->assertNull($this->p->junk);
    $this->p->name = 'Joe2';
    $this->assertEqual('Joe2', $this->p->name);
  }

  function testInsert() {
    $this->p->password = 'pw';
    $this->p->email = 'bob@example.com';
    $this->p->created = time();
    $this->assertTrue($this->p->insert());
    $this->assertEqual(1, $this->p->id);
    $this->assertFalse($this->p->insert());
    $this->assertNull($this->p->id);
  }

  function testSelect() {
    $this->p->select('select * from Users where id=1');
    $this->assertEqual('Joe2', $this->p->name);
    $this->assertEqual('pw', $this->p->password);
    $this->p->reset();
    $this->p->select('select * from Users where id=2');
    $this->assertNull($this->p->name);
    $this->assertNull($this->p->password);
    $this->p->select('select * from Users where name="Joe2"');
    $this->assertEqual('Joe2', $this->p->name);
    $this->assertEqual('pw', $this->p->password);
    $this->assertEqual(1, $this->p->id);
  }
}
