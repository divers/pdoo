<?php

require_once 'simpletest/autorun.php';
require_once 'datedstatement.class.php';

class TestOfPDOODatedStatement extends UnitTestCase {
  private $db;
  private $p;

  function __construct() {
    try {
      $this->db = new PDOO('sqlite::memory:');
      $this->db->exec(file_get_contents('tests/sqlite.test.schema'));
    } catch (PDOException $e) {
      echo $e->getMessage() . "\n";
    }
  }

  function testMake() {
    $this->p = $this->db->make('PDOODatedStatement');
    $this->assertEqual('PDOODatedStatement', get_class($this->p));
  }

  function testKey() {
    $this->assertEqual('id', $this->p->getKey());
  }

  function testColumns() {
    $this->assertEqual(array('created', 'modified', 'id'), $this->p->getColumns());
  }

  function testGetSet() {
    $this->assertNull($this->p->junk);
    $this->assertNull($this->p->created);
    $this->p->junk = true;
    $this->assertNull($this->p->junk);
    $this->p->created = 101010;
    $this->assertEqual(101010, $this->p->created);
  }

  function testInsert() {
    $this->p->created = 101010;
    $this->assertTrue($this->p->insert());
    $this->assertEqual(1, $this->p->id);
    $this->assertFalse($this->p->insert());
    $this->assertNull($this->p->id);
  }

  function testSelect() {
    $this->p->select('select * from PDOODatedStatement where id=1');
    $this->assertEqual(101010, $this->p->created);
    $this->p->reset();
    $this->p->select('select * from PDOODatedStatement where id=2');
    $this->assertNull($this->p->created);
    $this->p->select('select * from PDOODatedStatement where created=101010');
    $this->assertEqual(101010, $this->p->created);
    $this->assertEqual(1, $this->p->id);
  }
}
