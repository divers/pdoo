<?php

require_once 'pdoo.class.php';

class PDOODatedStatement extends PDOOStatement {
  protected function __construct($dbh, array $columns, $key = null) {
    //echo __METHOD__ . "\n";
    parent::__construct($dbh, $columns, $key);
  }

  static function defaultColumns() {
    return array('created', 'modified');
  }
}
