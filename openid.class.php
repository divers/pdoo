<?php

require_once 'datedstatement.class.php';

class OpenID extends PDOODatedStatement {
  protected function __construct($dbh, $columns, $key = null) {
    parent::__construct($dbh, $columns, $key);
  }

  static function defaultkey() {
    return 'user_id';
  }

  static function defaultColumns() {
    return array_merge(array('openid'), parent::defaultColumns());
  }
}
