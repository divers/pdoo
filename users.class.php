<?php

require_once 'datedstatement.class.php';

class Users extends PDOODatedStatement {
  protected function __construct($dbh, $columns, $key = null) {
    //echo __METHOD__ . "\n";
    parent::__construct($dbh, $columns, $key);
  }

  static function defaultColumns() {
    return array_merge(array('name', 'password', 'email'), parent::defaultColumns());
  }
}
