<?php

class nulltable {
}

class PDOO extends PDO {
  private $tables;

  function __construct($dsn, $username = null, $password = null, $driver_options = null) {
    parent::__construct($dsn, $username, $password, $driver_options);
    $this->tables = array();
  }

  function getTables() {
    return array_keys($this->tables);
  }

  function getTable($table) {
    if (isset($this->tables[$table])) return $this->tables[$table];
    return new nulltable;
  }

  function make($table) {
    if (isset($this->tables[$table])) return $this->tables[$table];
    $key = call_user_func(array($table, 'defaultKey'));
    $columns = call_user_func(array($table, 'defaultColumns'));
    /*echo "key:$key\n";
    echo 'columns:';
    var_dump($columns);*/
    $columns[] = $key;
    //var_dump($columns);
    $sql = "insert into $table (" . join(', ', $columns) . ') values ('
      . join(', ', array_map(create_function('$c', 'return ":$c";'), $columns)) . ')';
    //echo "sql:$sql\n";
    $t = $this->prepare($sql
      , array(PDO::ATTR_STATEMENT_CLASS => array($table, array($this, $columns, $key)))
    );
    //var_dump($t);
    if ($t) return $this->tables[$table] = $t;
    return new nulltable;
  }
}

abstract class PDOOStatement extends PDOStatement {
  // bindParam() needs this to be public but please refrain from accessing it directly
  // use PDOOStatement->name, PDOOStatement->password, etc.
  // and the __get()/__set() methods will take care of the rest
  public $data;

  protected $dbh;
  protected $key;
  protected $columns;

  abstract static function defaultColumns();

  protected function __construct($dbh, array $columns, $key = null) {
    //echo __METHOD__ . "\n";
    $this->dbh = $dbh;
    $this->key = $key;
    $this->data = array();
    $this->columns = $columns;
    $this->bindAll();
  }

  static function defaultKey() {
    return 'id';
  }

  function getKey() {
    return $this->key;
  }

  function getColumns() {
    return $this->columns;
  }

  function insert($parameters = null) {
    $ret = parent::execute($parameters);
    if ($a = $this->dbh->lastInsertId()) $this->data[$this->key] = (int)$a;
    if ((false === $ret) && isset($this->data[$this->key])) unset($this->data[$this->key]);
    return $ret;
  }

  protected function bindAll() {
      array_map(array($this, 'bindParam'), $this->columns);
  }

  function bindParam($parameter) {
    if ($parameter === $this->key)
      return parent::bindParam(":$parameter", $this->data[$parameter], PDO::PARAM_INT);
    if (in_array($parameter, $this->columns))
      return parent::bindParam(":$parameter", $this->data[$parameter]);
  }

  function fill() {
    return $this->selectWhere($this->data);
  }

  function selectWhere($search) {
    $cols = array();
    foreach ($search as $k => $v) if (!empty($v)) $cols[] = $k. ' = ' . $this->dbh->quote($v);
    $sql = 'select * from ' . get_class($this) . ' where ' . join(' and ', $cols);
    return $this->select($sql);
  }

  function select($sql) {
    $x = $this->dbh->query($sql, PDO::FETCH_INTO, $this);
    $x->fetch();
    return $x;
  }

  function __get($key) {
    if ('_key' === $key) $key = $this->key;
    if (isset($this->data[$key])) return $this->data[$key];
  }

  function __set($key, $value = null) {
    if ('_key' === $key) $key = $this->key;
    if (!in_array($key, $this->columns)) return;
    if (is_null($value)) $this->data[$key] = null;
    else $this->data[$key] = ($key === $this->key) ? (int)$value : $value;
  }

  function reset() {
    array_map(array($this, 'nullify'), $this->columns);
  }

  private function nullify($c) {
    $this->$c = null;
  }
}
