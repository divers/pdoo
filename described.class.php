<?php

require_once 'datedstatement.class.php';

class PDOODescribedStatement extends PDOODatedStatement {
  protected function __construct($dbh, $columns, $key = null) {
    parent::__construct($dbh, $columns, $key);
  }

  static function defaultColumns() {
    return array_merge(array('name', 'description'), parent::defaultColumns());
  }
}
